#!/bin/bash


echo "RSA 4096 GPG Key Generation and export"
echo "use your desired app email adress"
echo "password MUST have at least 12 characters length ,  one upperscore and underscore letter and one of the following characters: _ . - + "

echo "Full Name:";read name
echo "Full Email:";read mailto
echo "Passphrase(will not be shown):";read -s passphrase

if [[ "${mailto}" =~ ^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,25}$ ]]
then
    echo "Email address ${mailto} is valid."
else
    echo "Email address ${mailto} is invalid.";exit 1 
fi

passverify=`echo ${passphrase} | egrep "^.{12,255}" | egrep "[ABCDEFGHIJKLMNOPQRSTUVWXYZ]" | \
              egrep "[abcdefghijklmnopqrstuvwxyz"] | egrep "[0-9]"| \
              grep -v -e ABC -e BCD -e CDE -e DEF -e EFG -e FGH -e GHI -e HIJ -e IJK -e JKL -e KLM -e LMN -e MNO -e NOP -e OPQ -e PQR -e QRS -e RST -e STU -e TUV -e UVW -e VWX -e WXY -e XYZ -e abc -e bcd -e cde -e def -e efg -e fgh -e ghi -e hij -e ijk -e jkl -e klm -e lmn -e mno -e nop -e opq -e pqr -e qrs -e rst -e stu -e tuv -e uvw -e vwx -e wxy -e xyz |egrep "[_.-\+]"`
          if [ -z "$passverify" ];then
          echo "password is too weak";exit 2
           fi 



echo '%echo Generating a basic OpenPGP key
      Key-Type: RSA
      Key-Length: 4096
      Subkey-Type: RSA
      Subkey-Length: 4096
      Name-Real: '${name}'
      Name-Comment: no comment
      Name-Email: '${mailto}'
      Expire-Date: 0
      Passphrase: '${passphrase}'
      # Do a commit here, so that we can later print "done" :-)
      %commit
      %echo done' | gpg --batch --generate-key /dev/stdin && echo "exporting .. gpg may ask in a separate window " && gpg --output /tmp/armor.gpg --armor --export-secret-key "${mailto}"

echo "you can export your key manually with the following command:
gpg --output /tmp/armor.gpg --armor --export-secret-key ${mailto}"

