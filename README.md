# Openpgp Batch Creator.Sh

##### a batch openpgp 4096bit RSA GPG Key generator and exporter suitable for passbolt 

# Usage:

* open a shell and do not use fullscreen or foreground mode ( gpg password popup may be hidden )
* `git clone https://gitlab.com/the-foundation/openpgp-batch-creator.sh.git`
* `bash openpgp-batch-creator.sh/batch-gpg.sh` 
* the script will store your exported key in `/tmp/armor.gpg` 
* upload the armor.gpg file for recovery / account creation
* delete the file  ( rm /tmp/armor.gpg ) ( your key is still in your keyring)


# Warning:

! delete the exported file as soon as you don't need it anymore and backup your gpg keyring !
